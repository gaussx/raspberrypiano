import RPi.GPIO as gpio
from playsound import playsound
import musicalbeeps
import os
import sys
from time import sleep

sys.path.append("..")
import spidev as SPI
from lib import LCD_1inch8
from PIL import Image, ImageDraw, ImageFont

RST = 27
DC = 25
BL = 18
bus = 0
device = 0

disp = LCD_1inch8.LCD_1inch8()
font = ImageFont.truetype("/home/pi/raspberrypiano/Font/Font02.ttf", 20);
big_font = ImageFont.truetype("/home/pi/raspberrypiano/Font/Font02.ttf", 34);
free_piano_font = ImageFont.truetype("/home/pi/raspberrypiano/Font/Font02.ttf", 72);

NOTE_DURATION = 0.1
try:
    Lcd_ScanDir = LCD_1inch8.SCAN_DIR_DFT  #SCAN_DIR_DFT = D2U_L2R
    # Initialize library.
    disp.Init()
    # Clear display.
    disp.clear()
    #Set the backlight to 100
    disp.bl_DutyCycle(50)
except KeyboardInterrupt:
    disp.module_exit()
    exit()

class Key:
	PIN = 0
	sound = "/home/pi/raspberrypiano/assets/err.wav"
	def __init__(self, PIN, sound):
		self.PIN = PIN
		self.sound = sound

LOW_KEYS = [Key(20, "c.wav"), Key(21, "c#.wav"), Key(12, "d.wav"), Key(16, "d#.wav")]
MID_KEYS = [Key(19, "e.wav"), Key(26, "f.wav"), Key(6, "f#.wav"), Key(13, "g.wav")]
HIGH_KEYS = [Key(23, "g#.wav"), Key(24, "a.wav"), Key(17, "a#.wav"), Key(22, "b.wav")]

class Keyboard:
	keys = {}
	player = musicalbeeps.Player(volume = 1, mute_output = False)
	def play(self, key):
		#playsound(self.keys[key].sound)
		self.player.play_note(key, NOTE_DURATION)
	def __init__(self, LOW_KEYS, MID_KEYS, HIGH_KEYS):
		self.keys['C4'] = LOW_KEYS[0]
		self.keys['C4#'] = LOW_KEYS[1]
		self.keys['D4'] = LOW_KEYS[2]
		self.keys['D4#'] = LOW_KEYS[3]

		self.keys['E4'] = MID_KEYS[0]
		self.keys['F4'] = MID_KEYS[1]
		self.keys['F4#'] = MID_KEYS[2]
		self.keys['G4'] = MID_KEYS[3]

		self.keys['G4#'] = HIGH_KEYS[0]
		self.keys['A4'] = HIGH_KEYS[1]
		self.keys['A4#'] = HIGH_KEYS[2]
		self.keys['B4'] = HIGH_KEYS[3]

array = [[31, 33, 35, 37], [32, 36, 38, 40], [16, 18, 15, 11]]


#gpio.setmode(gpio.BOARD)
for el in LOW_KEYS:
	gpio.setup(el.PIN, gpio.IN, pull_up_down = gpio.PUD_DOWN)

for el in MID_KEYS:
	gpio.setup(el.PIN, gpio.IN, pull_up_down = gpio.PUD_DOWN)

for el in HIGH_KEYS:
	gpio.setup(el.PIN, gpio.IN, pull_up_down = gpio.PUD_DOWN)

keyboard = Keyboard(LOW_KEYS, MID_KEYS, HIGH_KEYS)
most_recent_el = " "
disp.Init()
disp.clear()
disp.bl_DutyCycle(50)

def teach_song(song_name):
    song = open(song_name, "r")
    notes = song.readlines()
    notes = [x[:-1].split() for x in notes]
    print(notes)
    most_recent_notes = []
    current_note_duration = 0
    current_note_played = 0
    last_recorded_note_played = 0
    el = ""
    while len(notes) > 0:
        image = Image.new("RGB", (disp.width, disp.height), "WHITE")
        draw = ImageDraw.Draw(image)
        most_recent_el = el
        
        if current_note_played > current_note_duration:
            notes = notes[1:]
        if len(notes) == 0:
            break
        if notes != most_recent_notes:
            current_note_duration = float(notes[0][1])
            current_note_played = 0
            disp.clear()
            draw.text((5, 0), "Zagraj:", fill = "BLUE", font = font)
            text = notes[0][0] + " przez " + notes[0][1] + " sek."
            draw.text((5, 20), text, fill = "RED", font = font)
            for i in range(1, min(5, len(notes))):
                text = notes[i][0] + " przez " + notes[i][1] + " sek."
                draw.text((5, 20 * i + 20), text, fill = "BLACK", font = font)
            disp.ShowImage(image)
            most_recent_notes = [x for x in notes]
        for el in Keyboard.keys:
                if gpio.input(Keyboard.keys[el].PIN) == gpio.HIGH:
                    #print("Nacisnieto:", el)
                    keyboard.play(el)
                    if el == notes[0][0]:
                        current_note_played += (NOTE_DURATION * 1.5)
    disp.clear()
    draw.text((5, 0), "GRATULACJE!!!", fill = "GOLD", font = big_font)
    disp.ShowImage(image)
    sleep(3)
while True:
    wybor = None
    disp.clear()
    image = Image.new("RGB", (disp.width, disp.height), "WHITE")
    draw = ImageDraw.Draw(image)
    draw.text((5, 00), "Wybierz tryb:", fill = "RED", font = font)
    draw.text((5, 25), "C: Kurki Trzy", fill = "BLACK", font = font)
    draw.text((5, 50), "C#: Stary Donald", fill = "BLACK", font = font)
    draw.text((5, 75), "D: Pada Snieg", fill = "BLACK", font = font)
    draw.text((5, 100), "D#: Dowolna gra", fill = "BLACK", font = font)
    disp.ShowImage(image)
    while wybor == None:
        for el in Keyboard.keys:
            if gpio.input(Keyboard.keys[el].PIN) == gpio.HIGH:
                wybor = el
    if wybor == 'C4':
        teach_song("/home/pi/raspberrypiano/songs/Twinkle_Twinkle_Little_Star.txt")
    elif wybor == 'C4#':
        teach_song("/home/pi/raspberrypiano/songs/Old_Macdonald.txt")
    elif wybor == 'D4':
        teach_song("/home/pi/raspberrypiano/songs/Jingle_Bells.txt")
    else:
        while True:
            for el in Keyboard.keys:
                if gpio.input(Keyboard.keys[el].PIN) == gpio.HIGH:
                    print("Nacisnieto:", el)
                    keyboard.play(el)
                    try:
                        if(most_recent_el != el):
                            disp.clear()
                            image = Image.new("RGB", (disp.width, disp.height), "WHITE")
                            draw = ImageDraw.Draw(image)
                            most_recent_el = el
                            draw.text((5, 10), el, fill = "BLACK", font = free_piano_font)
                            disp.ShowImage(image)
                    except:
                        print("GIGA ERROR")
                    os.system("clear")
